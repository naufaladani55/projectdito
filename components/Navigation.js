import { Navbar } from "flowbite-react";
import React from "react";

export default function Navigation() {
  return (
    <div className="xl:mx-auto xl:w-auto w-[650px] border shadow-md mt-2">
      <Navbar fluid={true} rounded={true}>
        <Navbar.Brand href="/">
          <img
            src="../assets/lipo-logo.png"
            className="mr-3 h-20 lg:h-24 "
            alt="Logo"
          />
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse>
          <Navbar.Link href="/cendara-essence">Cendana Essence</Navbar.Link>
          <Navbar.Link href="/">Colony Himalaya</Navbar.Link>
          <Navbar.Link href="/">Park Serpong</Navbar.Link>
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
}
