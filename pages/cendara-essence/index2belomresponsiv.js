import React, { useState } from "react";
import Layout from "@/widget/Layout";
import Link from "next/link";
import { Carousel } from "flowbite-react";

import autoprefixer from "autoprefixer";

export default function cendaraEssence() {
  const [showModal, setShowModal] = useState(false);
  const [selectedImage, setSelectedImage] = useState("");

  const handleImageClick = (image) => {
    setSelectedImage(image);
    setShowModal(!showModal); // Mengubah status modal menjadi negasi dari status sebelumnya
  };
  return (
    <Layout>
      <div>
        {/* div */}
        {/* <div className="lg:flex lg:flex-rows lg:gap-[710px] lg:justify-center md:flex md:flex-rows md:gap-72 md:justify-center mb-10">
          <div>
            <h1 className="mt-4 lg:mt-0 md:mt-0 text-2xl font-bold text-red-600">
              Cicilan Rp 5,5 Juta-an / Bulan
            </h1>
            <p className="text-md text-gray-600">KPR Mulai 794 Juta-an</p>
          </div>
        </div> */}
        {/* gambar */}
        <div className="grid grid-cols-2 gap-4 mt-5">
          <div className="flex items-center justify-center">
            <img
              className="max-w-full h-auto shadow-lg"
              src="../../assets/cendana/rumah1.jpg"
              alt=""
            />
          </div>

          <div className="flex items-center">
            <div>
              <h1 className="text-3xl font-bold text-gray-600 mt-[-100px] ml-7 mb-5">
                Cendana Essence
              </h1>
              <p className="mx-8 font-montserrat text-lg text-justify">
                Adalah Chapter ke-17 dari series Cendana Homes, Cendana Essence
                menghadirkan rumah modern yang menjadi idaman bagi keluarga
                tercinta. Dengan memberikan kenyamanan dan fasilitas Sekolah,
                Cendana Essence cocok bagi mereka yang memiliki mobilitas
                tinggi. Dengan fasilitas seperti Mall, Supermarket, Rumah Sakit,
                dan akses pintu Tol Lippo Karawaci yang dekat dari cluster,
                Cendana Essence memenuhi kebutuhan gaya hidup modern masa kini.
              </p>
            </div>
          </div>
        </div>

        {/* <div class="mt-9">
          <table>
            <tr>
              <td>
                <img
                  class="w-[650px]"
                  src="../../assets/cendana/rumah1.jpg"
                  alt=""
                />
              </td>
              <td class="pl-6">
                <p>
                  Cendana Essence, sebagai Chapter ke-17 dari seri Cendana
                  Homes, menghadirkan rumah modern yang menjadi idaman bagi
                  keluarga tercinta. Dengan memberikan kenyamanan dan fasilitas
                  lengkap, Cendana Essence cocok bagi mereka yang memiliki
                  mobilitas tinggi. Dengan fasilitas seperti Mall Supermaret,
                  Rumah Sakit, dan akses pintu Tol Lippo Karawaci yang dekat
                  dari cluster, Cendana Essence memenuhi kebutuhan gaya hidup
                  modern Anda
                </p>
              </td>
            </tr>
          </table>
        </div> */}

        {/* div promoterbatas */}
        <div className=" mt-12 lg:mt-6 bg-rose-100 py-2 px-4 ">
          <h1 className="text-2xl font-sans text-red-500 font-semibold">
            Program :
          </h1>
          <ul className="list-none ml-5 mt-5 text-gray-600">
            <li>&#10004; Free : Biaya Akad Bank</li>
            <li>&#10004; UTJ : 10 Juta</li>
            <li>&#10004; DP Minim : 2,5%</li>
            <li>
              &#10004; SMART HOME PACKAGE (CCTV, Smart Door, & Water Heater)
            </li>
          </ul>
        </div>

        {/* tambahkan carousel disini */}
        <div className="h-56 sm:h-64 xl:h-80 2xl:h-96 border shadow my-6   w-[90%] ml-[5%]">
          <Carousel className="bg-gray-200">
            {/* slide1 */}
            <div className="grid  grid-cols-1   ">
              <div className=" mt-[10px]">
                {/* gambar slide 1 */}
                <img
                  className="w-auto h-96  border shadow my-6"
                  src="../../assets/cendana/1.jpg"
                  onClick={() => handleImageClick("../../assets/cendana/1.jpg")}
                />
              </div>
            </div>

            {/* slide2 */}
            <div className="grid  grid-cols-1   ">
              <div className=" mt-[10px]">
                {/* gambar slide 1 */}
                <img
                  className="w-auto h-96  border shadow my-6"
                  src="../../assets/cendana/2.jpg"
                  onClick={() => handleImageClick("../../assets/cendana/2.jpg")}
                />
              </div>
            </div>
            {/* slide3 */}
            <div className="grid  grid-cols-1   ">
              <div className=" mt-[10px]">
                {/* gambar slide 1 */}
                <img
                  className="w-auto h-96 border shadow my-6"
                  src="../../assets/cendana/3.jpg"
                  onClick={() => handleImageClick("../../assets/cendana/3.jpg")}
                />
              </div>
            </div>
          </Carousel>
        </div>

        {/* div keunggulan */}
        <div className="text-gray-600">
          <h1 className="text-2xl font-semibold mt-5 mb-5">Surrounding Area</h1>
          <div className="px-6 grid grid-cols-2 lg:grid lg:grid-cols-3  mx-auto gap-10">
            {/* <div className="text-center justify-center ">
              <img
                className=" sm:ml-16 md:ml-24 lg:ml-10 xl:ml-16"
                src="../../assets/plane.png"
                alt="none"
              />
              <p className="font-semibold font-sans">
                20 Menit ke Bandara Soekarno Hatta
              </p>
            </div> */}
            <div className="text-center justify-center ">
              <img
                className=" sm:ml-16 md:ml-24 lg:ml-10 xl:ml-16 h-64"
                src="../../assets/cendana/rumah2.jpg"
                alt="none"
              />
              {/* <p className="font-semibold font-sans">
                25 Menit ke Stasiun Tangerang Tangcity Mall
              </p> */}
            </div>
            <div className="text-center justify-center ">
              <img
                className=" sm:ml-16 md:ml-24 lg:ml-10 xl:ml-16 h-64"
                src="../../assets/cendana/supermall.jpg"
                alt="none"
              />
            </div>
            <div className="text-center justify-center ">
              <img
                className=" sm:ml-16 md:ml-24 lg:ml-10 xl:ml-16 h-64"
                src="../../assets/cendana/maxboxnew.jpg"
                alt="none"
              />
            </div>
            <div className="text-center justify-center ">
              <img
                className=" sm:ml-16 md:ml-24 lg:ml-10 xl:ml-16 h-64"
                src="../../assets/cendana/tol.jpg"
                alt="none"
              />
            </div>
            <div className="text-center justify-center ">
              <img
                className=" sm:ml-16 md:ml-24 lg:ml-10 xl:ml-16 h-64"
                src="../../assets/cendana/siloam.jpg"
                alt="none"
              />
            </div>
            <div className="text-center justify-center ">
              <img
                className=" sm:ml-16 md:ml-24 lg:ml-10 xl:ml-16 h-64"
                src="../../assets/cendana/uph.jpg"
                alt="none"
              />
            </div>
          </div>
          {/* div wa */}
          <div>
            <div className="fixed bottom-0 right-0 mb-4 mr-4 z-50">
              <Link
                href={"https://wa.me/6285756245829"}
                target="_blank"
                rel="noopener noreferrer"
              >
                <button
                  type="button"
                  className="text-white bg-[#07C72F] hover:bg-gray-200/90 hover:text-[#07C72F] border-2 border-[#07C72F] focus:ring-4 focus:outline-none focus:ring-[#07C72F]/50 font-medium rounded-lg text-sm px-2 py-2.5 text-center inline-flex items-center dark:focus:ring-[#07C72F]/55 mr-2 mb-2"
                >
                  <img
                    className="bg-white hover:visible bg-transparent h-6 w-6 mx-2"
                    src="../../assets/wa.svg"
                    alt=""
                  />
                  <span className="text-xl">Info dan Reservasi Pesanan</span>
                </button>
              </Link>
            </div>

            {/* div maps */}
            <div>
              <div className="mb-5 text-xl "></div>
              <div>
                <h1 className="text-3xl font-bold ">Temukan Kami</h1>

                <iframe
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.3064972747525!2d106.58293097418274!3d-6.223256860948336!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69ff895ade05ef%3A0xfd8286c2ebcaf41!2sCendana%20Essence!5e0!3m2!1sid!2sid!4v1715524025145!5m2!1sid!2sid"
                  width="100%"
                  height="300px"
                  style={{ border: 0 }}
                  allowFullScreen
                  loading="lazy"
                  referrerPolicy="no-referrer-when-downgrade"
                />
              </div>
            </div>
            {showModal && (
              <div
                className="fixed inset-0 z-50 overflow-y-auto bg-gray-900 bg-opacity-50 flex justify-center items-center"
                onClick={handleImageClick}
              >
                <div className="relative p-4 max-w-full">
                  <button className="absolute top-0 right-0 m-4 text-white text-xl focus:outline-none">
                    &#x2715;
                  </button>
                  <img
                    className="w-full h-full"
                    src={selectedImage}
                    alt="Full screen"
                  />
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
}
