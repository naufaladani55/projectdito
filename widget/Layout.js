import Navigation from "../components/Navigation";
import Footer from "../components/Footer";

export default function Layout({ children }) {
  return (
    <>
      <Navigation />
      {/* content */}
      <div className="container mx-auto p-5">{children}</div>
      {/* content */}
      <Footer />
    </>
  );
}
